// Author: Thorbjoern

#include<iostream>
#include<string>
#include<random>

std::random_device randomDevice;     
std::mt19937 randomEngine(randomDevice());    
std::uniform_int_distribution<int> uniformDistribution(17,231); 

// \033[38;5;<N>m   ,   <N> = 17 - 231
// https://en.wikipedia.org/wiki/ANSI_escape_code
std::string add_color(const int &color)
{
    std::string text = "\033[38;5;"+std::to_string(color)+"m";
    return text;
}

void print_color(const std::string &text)
{
    int color;
    for(int j=0; j<text.size();j++)
    {
        color = uniformDistribution(randomEngine);
        std::cout<<add_color(color)<<text[j];
    }
    std::cout<<" "; 
}

int main(int argc, char* argv[])
{
    std::string text;
    if(argc>1)
    {
        for(int i=1; i<argc; i++)
        {
            text = argv[i];
            print_color(text);
        }
        std::cout<<"\033[0m"<<std::endl;
    }
    return 0;
}
