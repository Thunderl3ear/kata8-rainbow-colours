# Kata 8 - A rainbow color printer
Can be called with any number of arguments. The arguments are printed in the console with random colors for each letter.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
* Requires `gcc`.

## Usage
* mkdir build
* Compile with: g++ main.cpp -o build/rainbow
* cd build
* Run: ./rainbow

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
